<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$subjectivity = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->SubjectivityAnalysis($tweet);
	if (!isset($subjectivity[$result])) {
	    $subjectivity[$result] = 0;
	}
	$subjectivity[$result]++;
}

unset($DatumboxAPI);

print json_encode($subjectivity);